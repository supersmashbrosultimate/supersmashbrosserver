package com.supersmash.supersmashbrosserver.Controllers;

import com.supersmash.supersmashbrosserver.Models.GamesModel;
import com.supersmash.supersmashbrosserver.Repositories.GamesRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@CrossOrigin
@RestController
public class GameController {
    private final GamesRepository gameRepo;

    public GameController(GamesRepository gameRepo) {
        this.gameRepo = gameRepo;
    }

    @GetMapping("/gamesUniverse")
    public List<GamesModel> getAllGames() { return (List<GamesModel>) this.gameRepo.findAll(); }
    

}

package com.supersmash.supersmashbrosserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SupersmashbrosserverApplication {

    public static void main(String[] args) {
        SpringApplication.run(SupersmashbrosserverApplication.class, args);
    }

}

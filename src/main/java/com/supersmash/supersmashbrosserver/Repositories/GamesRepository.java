package com.supersmash.supersmashbrosserver.Repositories;

import com.supersmash.supersmashbrosserver.Models.GamesModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface GamesRepository extends CrudRepository<GamesModel, Long> {

}


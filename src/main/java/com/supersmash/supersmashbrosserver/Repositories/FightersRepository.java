package com.supersmash.supersmashbrosserver.Repositories;

import com.supersmash.supersmashbrosserver.Models.FightersModel;
import com.supersmash.supersmashbrosserver.Models.GamesModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface FightersRepository extends CrudRepository<FightersModel, Long> {
    List<FightersModel> getAllByGame_GameName(String game);

    @Query("select f.image from FightersModel f")
    List<String> getAllImages();
}

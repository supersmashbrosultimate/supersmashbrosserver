package com.supersmash.supersmashbrosserver.Service;

import com.supersmash.supersmashbrosserver.Models.FightersModel;
import com.supersmash.supersmashbrosserver.Repositories.FightersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FightersTestService {

    @Autowired
    private FightersRepository fightersRepo;

    public Iterable<FightersModel> getAllFighters() {
        return fightersRepo.findAll();
    }
}

package com.supersmash.supersmashbrosserver.Models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class GamesModel {

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String gameName;

    @Column
    private String gameSymbol;

    @Column
    private String gameColor;

}

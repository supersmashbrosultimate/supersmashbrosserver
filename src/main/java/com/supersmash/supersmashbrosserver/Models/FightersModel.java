package com.supersmash.supersmashbrosserver.Models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Setter
@Getter
public class FightersModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @Column
    private String image;

    @Column
    private String carouselImage;

    @JoinColumn(name="game", referencedColumnName = "id")
    @ManyToOne
    private GamesModel game;

    @Column
    private String description;

    @Column
    private String tier;

}

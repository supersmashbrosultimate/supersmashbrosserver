package com.supersmash.supersmashbrosserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Primary;

@SpringBootConfiguration
@EnableAutoConfiguration
public class SuperSmashBrosServerTestConfiguration {

    @Primary
    public static void main(String[] args) {SpringApplication.run(SupersmashbrosserverApplication.class, args); }
}

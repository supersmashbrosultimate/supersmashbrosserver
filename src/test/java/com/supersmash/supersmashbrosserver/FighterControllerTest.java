package com.supersmash.supersmashbrosserver;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.supersmash.supersmashbrosserver.Controllers.FightersController;

import com.supersmash.supersmashbrosserver.Models.FightersModel;
import com.supersmash.supersmashbrosserver.Service.FightersTestService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.internal.configuration.MockAnnotationProcessor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@WebMvcTest(value = FightersController.class)
@ContextConfiguration(classes = {SuperSmashBrosServerTestConfiguration.class})
public class FighterControllerTest{
    @Autowired
    private MockMvc mockMvc;

    @InjectMocks
    private FightersController fightersController;

    @MockBean
    private FightersTestService fightersTestService;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        this.mockMvc = MockMvcBuilders.standaloneSetup(fightersController).build();
    }

    @Test
    public void getAllFighters() throws Exception {
        FightersModel fightersModel = new FightersModel();
        fightersModel.setName("Joker");
        fightersModel.setId(1L);
        fightersModel.setDescription("new Character");

        FightersModel fightersModel2 = new FightersModel();
        fightersModel2.setId(2L);
        fightersModel2.setName("Chris");
        fightersModel2.setDescription("fighter 2");

        List<FightersModel> fightersList = new ArrayList<>();
        fightersList.add(fightersModel);
        fightersList.add(fightersModel2);

        Mockito.when(fightersTestService.getAllFighters()).thenReturn(fightersList);
        String URI = "/fighters";

        RequestBuilder requestBuilder = MockMvcRequestBuilders.get(URI).accept(MediaType.APPLICATION_JSON);

        MvcResult result = mockMvc.perform(requestBuilder).andReturn();

        String expectedJson = this.mapToJson(fightersList);
        String outputInJson = result.getResponse().getContentAsString();
        assertThat(outputInJson).isEqualTo(expectedJson);
    }

    private String mapToJson(Object object) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writeValueAsString(object);
    }

}
